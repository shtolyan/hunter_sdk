public class UpdateSpan
{
    public float TimePassed { get; set; }

    public bool CanDoAction { get; protected set; }
    public float Interval { get; set; }

    public bool AutoReset { get; set; }

    public UpdateSpan()
    {
    }

    public UpdateSpan(float interval)
    {
        Interval = interval;
    }

    public UpdateSpan(float interval, bool autoReset)
    {
        Interval = interval;
        AutoReset = autoReset;
    }

    public UpdateSpan(float interval, bool autoReset, bool CanDoActionAtStart)
    {
        Interval = interval;
        AutoReset = autoReset;
        CanDoAction = CanDoActionAtStart;
    }

    public void Tick(float time)
    {
        if (TimePassed > Interval)
        {
            if(AutoReset)
                TimePassed -= Interval;
            CanDoAction = true;
        }
        else
        {
            TimePassed += time;
            CanDoAction = false;
        }
    }

    public void Reset()
    {
        TimePassed = 0;
        CanDoAction = false;
    }
}