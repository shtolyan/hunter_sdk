﻿using UnityEngine;

public class BoundsMeasure : MonoBehaviour {
    public Bounds GetBounds() {
        return GetBounds(this.gameObject);
    }
    private Bounds GetBounds(GameObject objeto) {
        Bounds bounds;
        Renderer childRender;
        bounds = getRenderBounds(objeto);
        if (bounds.extents.x == 0) {
            bounds = new Bounds(objeto.transform.position, Vector3.zero);
            foreach (Transform child in objeto.transform) {
                childRender = child.GetComponent<Renderer>();
                if (childRender) {
                    bounds.Encapsulate(childRender.bounds);
                }
                bounds.Encapsulate(GetBounds(child.gameObject));
            }
        }
        return bounds;
    }

    Bounds getRenderBounds(GameObject objeto) {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        Renderer render = objeto.GetComponent<Renderer>();
        if (render != null) {
            return render.bounds;
        }
        return bounds;
    }
}
