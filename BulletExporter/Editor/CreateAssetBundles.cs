﻿using _lib_Unity;
using MicroModelServer.DataTypes;
using Newtonsoft.Json;
using Sharp3D.Math.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vega.Model3D;

public class CreateAssetBundles
{
    private static readonly BuildAssetBundleOptions buildSettings = BuildAssetBundleOptions.UncompressedAssetBundle
        //| BuildAssetBundleOptions.ForceRebuildAssetBundle
        //| BuildAssetBundleOptions.UncompressedAssetBundle
        //| BuildAssetBundleOptions.StrictMode
        ;


    [MenuItem("Zen Modules/Data exporter/Export current scene data")]
    static void BuildCurrentSceneAssetBundls()
    {
        ExportCurrentSceneMetaData();
        TerrainMetadataExporter.ExportMetadata();
        TerrainImageExporter.ExportTerrainPicture();
    }

    private static void ExportCurrentSceneMetaData()
    {
        var outputPath = Path.GetFullPath(Constants.TerrainsExportPath);

        if (!Directory.Exists(outputPath))
        {
            Directory.CreateDirectory(outputPath);
        }

        var currentScene = SceneManager.GetActiveScene();
        var currentSceneName = currentScene.name.ToLower();
        Debug.Log(string.Join(",", AssetDatabase.GetAllAssetBundleNames()));
        var builds = AssetDatabase.GetAllAssetBundleNames()
            .Where(name => name.ToLower().Contains(currentSceneName))
            .Where(name => AssetDatabase.GetAssetPathsFromAssetBundle(name).Length > 0)
            .Select(name => CreateAssetBundleBuild(name))
            .ToArray();

        for (int i = 0; i < builds.Length; i++)
        {
            builds[i].assetBundleVariant += ".bundle";
        }

        var manifest =
            BuildPipeline.BuildAssetBundles(outputPath, builds, buildSettings, BuildTarget.StandaloneWindows64);
        Debug.Log("Build AssetBundles Done: " + manifest.GetAllAssetBundles().Length);
    }


    [MenuItem("Zen Modules/Data exporter/Export target data")]
    static void BuildHumanAssetBundles()
    {
        var outputPath = Path.GetFullPath(Constants.HumansExportPath);
        if (!Directory.Exists(outputPath))
        {
            Directory.CreateDirectory(outputPath);
        }

        Debug.Log(string.Join(",", AssetDatabase.GetAllAssetBundleNames()));
        var builds = AssetDatabase.GetAllAssetBundleNames()
            .Where(name => name.StartsWith("humans"))
            .Where(name => AssetDatabase.GetAssetPathsFromAssetBundle(name).Length > 0)
            .Select(name => CreateAssetBundleBuild(name))
            .ToArray();

        for (int i = 0; i < builds.Length; i++)
        {
            builds[i].assetBundleVariant += ".bundle";
        }

        var manifest =
            BuildPipeline.BuildAssetBundles(outputPath, builds, buildSettings, BuildTarget.StandaloneWindows64);

        foreach (var build in builds)
        {
            var asset = AssetDatabase.LoadAssetAtPath<GameObject>(build.assetNames.FirstOrDefault());
            asset.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);

            var locKey = string.Format("AssetUnit/{0}.{1}", build.assetBundleName, build.assetBundleVariant);
            var locComponent = asset.GetComponent<AssetLocalization>();
            var locName = locComponent != null ? locComponent.LocName : ("obj_" + asset.name);
            var locString = CreateLocString(locKey, locName);
            File.WriteAllText(
                string.Format("{0}/Languages/ru/actor_{1}.{2}.csv", Constants.EditorDataPath, build.assetBundleName,
                    build.assetBundleVariant), locString);


            var actorTypeFilename = string.Format("{0}/ActorType/{1}.{2}.json", Constants.EditorDataPath,
                build.assetBundleName, build.assetBundleVariant.Replace(".bundle", ""));
            ActorTypeJson actorType = null;
            if (File.Exists(actorTypeFilename))
            {
                string path = "";
                try
                {
                    var actorTypeArr =
                        JsonConvert.DeserializeObject<ActorTypeJson[]>((File.ReadAllText(actorTypeFilename)));
                    actorType = actorTypeArr[0];
                }
                catch (Exception ex)
                {
                    Debug.LogWarning("ActorType file exist but canot be read. Will be overwritten!!. Path: " + path);
                    actorType = null;
                }
            }

            if (actorType == null)
            {
                actorType = new ActorTypeJson()
                {
                    IconSizeMeters = 0,
                    IsIconResizable = false,
                    IsIconRotating = true,
                    WayPointCommands = new string[]
                    {
                        "Wait", "Hide", "Show", "WaitHitTo", "ShootAsEnemy", "HumanMove", "HumanLay", "HumanSit",
                        "HumanStand", "Kill"
                    },
                    Data = new ActorTypeJsonData[] { }
                };
            }

            actorType.Category = build.assetBundleVariant.Contains("civil") ? "CivilHuman" : "MilitaryHuman";
            actorType.Prefab = locKey;

            File.WriteAllText(actorTypeFilename, JsonConvert.SerializeObject(new ActorTypeJson[] {actorType}));
        }

        Debug.Log("Build Humans AssetBundles Done: " + manifest.GetAllAssetBundles().Length);
    }


    private static string CreateLocString(string locKey, string locValue)
    {
        return string.Format("Key;Value\n{0};{1}", locKey, locValue);
    }

    private static AssetBundleBuild CreateAssetBundleBuild(string name)
    {
        var result = new AssetBundleBuild();
        result.assetBundleName = name.Substring(0, name.IndexOf('.'));
        result.assetBundleVariant =
            AssetDatabase.GetImplicitAssetBundleVariantName(AssetDatabase.GetAssetPathsFromAssetBundle(name).First());
        result.assetNames = AssetDatabase.GetAssetPathsFromAssetBundle(name);
        Debug.LogFormat("assetBundleName={0}\nassetBundleVariant={1}\nassetNames={2}", result.assetBundleName,
            result.assetBundleVariant, result.assetNames);
        return result;
    }
}

public class ActorTypeJson
{
    public string Prefab { get; set; }
    public string Category { get; set; }
    public bool IsIconRotating { get; set; }
    public bool IsIconResizable { get; set; }
    public double IconSizeMeters { get; set; }
    public string[] WayPointCommands { get; set; }
    public ActorTypeJsonData[] Data { get; set; }
}

public class ActorTypeJsonData
{
    public string VariableKey { get; set; }
    public string VariableType { get; set; }
    public string VariableDefault { get; set; }
}