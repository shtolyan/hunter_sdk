﻿using MicroModelServer.DataTypes;
using Sharp3D.Math.Core;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class TerrainMetadataExporter
{
    public static void ExportMetadata()
    {
        var metadata = new List<TerrainMetadata>() { CreateMetadata(Terrain.activeTerrain) };
        var json = Newtonsoft.Json.JsonConvert.SerializeObject(metadata, new Vector2DConverterFacade(),
            new Vector3DConverterFacade(), new QuaternionDConverterFacade());

        var path = string.Format("{0}/{1}.{2}.json", Constants.TerrainsExportPath, Constants.AssetBundleName,
            SceneManager.GetActiveScene().name.ToLowerInvariant());
        var fullPaht = Path.GetFullPath(path);
        File.WriteAllText(fullPaht, json);
        Debug.Log("Metadata saved: " + fullPaht);

        // Export default camera transform
        var camera = Resources.FindObjectsOfTypeAll<Camera>()
            .First(c => c.gameObject.scene == SceneManager.GetActiveScene());
        var cameraPosition = new Vector3D(camera.transform.position.x, 0.0, camera.transform.position.z);
        var cameraRotation = new Vector3D(0.0, camera.transform.rotation.eulerAngles.y, 0.0);
        var cameraTransformData = new Vector3D[] { cameraPosition, cameraRotation };
        var cameraTransformDataJson =
            Newtonsoft.Json.JsonConvert.SerializeObject(cameraTransformData, new Vector3DConverterFacade());
        var cameraTransformDataPath = string.Format("{0}/{1}.{2}.cam", Constants.TerrainsExportPath,
            Constants.AssetBundleName, SceneManager.GetActiveScene().name.ToLowerInvariant());
        var cameraTransformDataFullPath = Path.GetFullPath(cameraTransformDataPath);
        File.WriteAllText(cameraTransformDataFullPath, cameraTransformDataJson);
        Debug.Log("default camera transform saved: " + cameraTransformDataFullPath);
    }

    public static TerrainMetadata CreateMetadata(Terrain terrain)
    {
        Bounds? meshTerrainBounds = null;
        if (terrain == null)
        {
            Debug.LogWarning("ExportMetadata terrain == NULL");
            if (!Terrain.activeTerrains.Any())
            {
                var bm = GameObject.FindObjectOfType<BoundsMeasure>();
                meshTerrainBounds = bm.GetBounds();
            }
        }

        TerrainMetadata metadata = new TerrainMetadata();
        var prefabName = string.Format("{0}.{1}", Constants.AssetBundleName,
            SceneManager.GetActiveScene().name.ToLowerInvariant()) + ".bundle";
        
        metadata.Prefab = prefabName;

        if (meshTerrainBounds.HasValue)
        {
            metadata.SizeMeters = new Vector2D(meshTerrainBounds.Value.size.x, meshTerrainBounds.Value.size.z);
            metadata.ZeroPointShiftMeters =
                new Vector2D(meshTerrainBounds.Value.extents.x, meshTerrainBounds.Value.extents.z);
        }
        else
        {
            var terrainsMinX = Terrain.activeTerrains.Select(t => t.GetPosition().x).Min();
            var terrainsMinZ = Terrain.activeTerrains.Select(t => t.GetPosition().z).Min();
            var terrainsMaxX = Terrain.activeTerrains.Select(t => t.GetPosition().x + GetTerrainSize(t).X).Max();
            var terrainsMaxZ = Terrain.activeTerrains.Select(t => t.GetPosition().z + GetTerrainSize(t).Y).Max();
            metadata.SizeMeters = new Vector2D(terrainsMaxX - terrainsMinX, terrainsMaxZ - terrainsMinZ);

            metadata.ZeroPointShiftMeters = new Vector2D(-terrainsMinX, -terrainsMinZ);
        }

        var cameraGameObject = Resources.FindObjectsOfTypeAll(typeof(GameObject))
            .Cast<GameObject>()
            .Select(go => go.GetComponentInChildren<Camera>(true))
            .First(c => c != null);
        var cameraPosition = cameraGameObject.transform.position;
        metadata.CameraDefaultPosition = new Vector2D(cameraPosition.x, cameraPosition.z);

        metadata.CameraDefaultZoom = 1000;

        return metadata;
    }

    private static Vector2D GetTerrainSize(Terrain terrain)
    {
        int w = terrain.terrainData.heightmapResolution;
        int h = terrain.terrainData.heightmapResolution;
        Vector3 meshScale = terrain.terrainData.size;
        meshScale = new Vector3(meshScale.x / (w - 1), meshScale.y, meshScale.z / (h - 1));

        var size = Vector3.Scale(meshScale, new Vector3(w - 1, 0, h - 1));
        return new Vector2D(Mathf.Abs(size.x), Mathf.Abs(size.z));
    }
}

public class TerrainMetadata
{
    public string Prefab { get; set; }
    public Vector2D SizeMeters { get; set; }
    public Vector2D ZeroPointShiftMeters { get; set; }
    public Vector2D CameraDefaultPosition { get; set; }
    public double CameraDefaultZoom { get; set; }
}
