﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using System.IO;
using Vega.Model3D;
using System.Collections.Generic;
using System.Linq;
using Sharp3D.Math.Core;
using MicroModelServer.DataTypes;

public class TerrainCollisionExporter : ScriptableObject
{
    public static void ExportCurrentSceneTerrainData() {
        Debug.Log("Exporting terrain data as mesh");

        var terrainDatas = new List<TerrainMeshData>();

        foreach (var terrain in Terrain.activeTerrains) {
            int w = terrain.terrainData.heightmapResolution;
            int h = terrain.terrainData.heightmapResolution;
            Vector3 meshScale = terrain.terrainData.size;
            meshScale = new Vector3(meshScale.x / (w - 1), meshScale.y, meshScale.z / (h - 1));
            Debug.Log(meshScale);
            //Vector2 uvScale = new Vector2(1.0f / (w - 1), 1.0f / (h - 1));

            float[,] tData = terrain.terrainData.GetHeights(0, 0, w, h);

            Debug.Log(string.Format("tData[{0},{1}]", w, h));

            const int _SUBTERRAIN_SIZE = 128;
            //const int _CHUNK_SIZE = _SUBTERRAIN_SIZE + 1;

            int _OPT_SIZE = SceneManager.GetActiveScene().name.Contains("4000") ? 8 : 1;
            int _SUBTERRAIN_SIZE_OPT = _SUBTERRAIN_SIZE / _OPT_SIZE;
            int _CHUNK_SIZE_OPT = _SUBTERRAIN_SIZE_OPT + 1;

            for (int subtX = 0; subtX < w / _SUBTERRAIN_SIZE; subtX++) {
                for (int subtY = 0; subtY < h / _SUBTERRAIN_SIZE; subtY++) {
                    Debug.Log(string.Format("subt [{0},{1}]", subtX, subtY));

                    Vector3[] tVertices = new Vector3[_CHUNK_SIZE_OPT * _CHUNK_SIZE_OPT];

                    // Build vertices
                    for (int ty = 0; ty < _CHUNK_SIZE_OPT; ty++) {
                        for (int tx = 0; tx < _CHUNK_SIZE_OPT; tx++) {
                            var x = tx * _OPT_SIZE + subtX * _SUBTERRAIN_SIZE;
                            var y = ty * _OPT_SIZE + subtY * _SUBTERRAIN_SIZE;
                            tVertices[ty * _CHUNK_SIZE_OPT + tx] = Vector3.Scale(meshScale, new Vector3(-y, tData[x, y], x));
                        }
                    }

                    int[] tPolys;
                    tPolys = new int[_SUBTERRAIN_SIZE_OPT * _SUBTERRAIN_SIZE_OPT * 6];

                    int index = 0;
                    // Build triangle indices: 3 indices into vertex array for each triangle
                    for (int y = 0; y < _SUBTERRAIN_SIZE_OPT; y++) {
                        for (int x = 0; x < _SUBTERRAIN_SIZE_OPT; x++) {
                            // For each grid cell output two triangles
                            tPolys[index++] = (y * _CHUNK_SIZE_OPT) + x;
                            tPolys[index++] = ((y + 1) * _CHUNK_SIZE_OPT) + x;
                            tPolys[index++] = (y * _CHUNK_SIZE_OPT) + x + 1;

                            tPolys[index++] = ((y + 1) * _CHUNK_SIZE_OPT) + x;
                            tPolys[index++] = ((y + 1) * _CHUNK_SIZE_OPT) + x + 1;
                            tPolys[index++] = (y * _CHUNK_SIZE_OPT) + x + 1;
                        }
                    }

                    // Optimize mesh
                    

                    terrainDatas.Add(new TerrainMeshData() {
                        Position = new Vector3D(
                            terrain.GetPosition().x,
                            terrain.GetPosition().y,
                            terrain.GetPosition().z
                            ),
                        MeshData = new Mesh3D() {
                            Name = string.Format("{0}_{1}_{2}", terrain.name, subtX, subtY),
                            Vertices = tVertices.Select(v => new Vector3D(-v.x, v.y, v.z)).ToArray(),
                            Triangles = tPolys,
                            TextureCoordinates = null,
                            Normals = null,
                            SubMeshes = new Mesh3D.SubMesh[] { new Mesh3D.SubMesh() { Count = tPolys.Length, MaterialName = "Default", Offset = 0 } },
                        },
                    });
                }
            }
        }

        SaveTerrainDataToFile(terrainDatas.ToArray(), string.Format("{0}.{1}.heights", Constants.AssetBundleName, SceneManager.GetActiveScene().name.ToLowerInvariant()));

        Debug.Log("Done");
    }

    private static void SaveTerrainDataToFile(object data, string filename) {
        var outputPath = Path.GetFullPath(Constants.TerrainsExportPath);
        if (!Directory.Exists(outputPath)) {
            Directory.CreateDirectory(outputPath);
        }

        var fullPath = Path.Combine(outputPath, filename);
        Debug.Log("Saving terrain " + filename + "\nFull: " + fullPath);
        var json = Newtonsoft.Json.JsonConvert.SerializeObject(data, new Vector3DConverterFacade(), new QuaternionDConverterFacade(), new Vector2DConverterFacade());
        File.WriteAllText(fullPath, json);
    }
}
