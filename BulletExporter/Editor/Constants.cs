﻿public static class Constants
{
    public const string AssetBundleName = "terrain2";
    public const string TerrainsExportPath = "C:/_Sim/Vega_Assets/Terrain";
    public const string HumansExportPath = "C:/_Sim/Vega_Assets/AssetUnit";
    public const string SceneObjectsExportPath = "C:/_Sim/Vega_Assets/AssetSceneObject";
    public const string VehiclesExportPath = "C:/_Sim/Vega_Assets/AssetUnit";
    public const string EditorDataPath = "C:/_Sim/Vega_Assets/Editor";
}