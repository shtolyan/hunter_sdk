﻿using _lib_Model3D.Ragdoll;
using MicroModelServer.DataTypes;
using Newtonsoft.Json;
using Sharp3D.Math.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using Vega.Model3D;

public class ExportRagdollCollision : ScriptableObject
{

    [MenuItem("Zen Modules/BulletExporter/Export ragdoll collision")]
    public static void ExportRagdollCollisionForSelected() {
        var rootTransform = Selection.activeTransform;
        Assert.IsNotNull(rootTransform, "rootTransform is null");

        var result = DoExportRagdollCollision(rootTransform);
        Debug.Log(result);
    }

    public static string DoExportRagdollCollision(Transform rootTransform) {
        var ragdollData = new List<RagdollCollisionElement>();
        foreach (var rigidbody in rootTransform.GetComponentsInChildren<Rigidbody>()) {
            ragdollData.Add(new RagdollCollisionElement() {
                Rigidbody = rigidbody,
                Joint = rigidbody.GetComponent<Joint>(),
                Colliders = rigidbody.transform
                    .GetComponentsInChildren<Collider>()
                    .Where(c => c.transform.parent == rigidbody.transform)
                    .ToList(),
            });
        }

        if (ragdollData.Count<1 || ragdollData.All(rd => rd.Joint == null)) {
            return null;
        }

        var result = new List<RagdollCollisionElementData>();
        foreach (var data in ragdollData) {
            var newElement = new RagdollCollisionElementData();
            newElement.RagdollElementName = data.Rigidbody.name;
            newElement.RigidBodyMass = data.Rigidbody.mass;
            newElement.RigidbodyLocalPosition = rootTransform.InverseTransformPoint(data.Rigidbody.transform.position).ToVector3D();
            newElement.RigidbodyLocalRotation = (Quaternion.Inverse(rootTransform.rotation) * data.Rigidbody.transform.rotation).ToQuaternionD();
            //newElement.RigidbodyLocalRotation = data.Rigidbody.transform.localRotation.ToQuaternionD();

            var characterJoint = data.Joint as CharacterJoint;
            if (characterJoint !=null ) {
                newElement.ParentRigidBodyName = characterJoint.connectedBody != null ? characterJoint.connectedBody.name : null;

                newElement.JointAnchor = characterJoint.anchor.ToVector3D();
                newElement.JointAxis = characterJoint.axis.normalized.ToVector3D();
                newElement.JointConnectedAnchor = characterJoint.connectedAnchor.ToVector3D();
                newElement.JointSwingAxis = characterJoint.swingAxis.normalized.ToVector3D();

                newElement.JointLowTwistLimit = characterJoint.lowTwistLimit.limit;
                newElement.JointHighTwistLimit = characterJoint.highTwistLimit.limit;
                newElement.JointSwing1Limit = characterJoint.swing1Limit.limit;
                newElement.JointSwing2Limit = characterJoint.swing2Limit.limit;
            }

            var hingeJoint = data.Joint as HingeJoint;
            if (hingeJoint != null) {
                newElement.ParentRigidBodyName = hingeJoint.connectedBody != null ? hingeJoint.connectedBody.name : null;

                newElement.JointAnchor = hingeJoint.anchor.ToVector3D();
                newElement.JointAxis = hingeJoint.axis.normalized.ToVector3D();
                newElement.JointConnectedAnchor = hingeJoint.connectedAnchor.ToVector3D();
                newElement.JointSwingAxis = Vector3D.Zero;

                newElement.JointLowTwistLimit = hingeJoint.limits.min;
                newElement.JointHighTwistLimit = hingeJoint.limits.max;
            }

            newElement.RagdollColliderDatas = data.Colliders.Select(c => {
                var ragdollColliderData = new RagdollColliderData() {
                    Name = c.name,
                    LocalPosition = c.transform.localPosition.ToVector3D(),
                    LocalRotation = c.transform.localRotation.ToQuaternionD(),
                };

                var box = c as BoxCollider;
                if (box != null) {
                    ragdollColliderData.IsBox = true;
                    ragdollColliderData.Data = box.size.ToVector3D();
                }

                var capsule = c as CapsuleCollider;
                if (capsule != null) {
                    ragdollColliderData.IsCapsule = true;
                    ragdollColliderData.Data = new Vector3D(capsule.radius, capsule.height, -100500);
                }

                var sphere = c as SphereCollider;
                if (sphere != null) {
                    ragdollColliderData.IsSphere = true;
                    ragdollColliderData.Data = new Vector3D(sphere.radius, -100500, -100500);
                }

                
                var mesh = c as MeshCollider;
                if (mesh != null) {

                    var meshData = mesh.sharedMesh;

                    var mesh3D = new Mesh3D()
                    {
                        Name = meshData.name,
                        Normals = meshData.normals.Select(n => new Vector3D(n.x, n.y, n.z)).ToArray(),
                        Triangles = meshData.triangles,
                        Vertices = meshData.vertices.Select(v => new Vector3D(v.x, v.y, v.z)).ToArray(),
                        TextureCoordinates = null,
                        SubMeshes = null,
                    };
                    
                    ragdollColliderData.IsMesh = true;
                    ragdollColliderData.IsConvexMesh = mesh.convex;
                    ragdollColliderData.Mesh3D = mesh3D;                    
                }

                return ragdollColliderData;
            }).ToList();

            result.Add(newElement);
        }

        var resultJson = JsonConvert.SerializeObject(result, new Vector2DConverterFacade(), new Vector3DConverterFacade(), new QuaternionDConverterFacade());
        Debug.Log('"'+EscapeIt(resultJson)+'"');
        File.WriteAllText("ExportedRagdoll.txt",resultJson);
        File.WriteAllText("EscapeIt_ExportedRagdoll.txt",'"'+EscapeIt(resultJson)+'"');
        return resultJson;
    }

    static string EscapeIt(string value) {
        var builder = new StringBuilder();
        foreach (var cur in value) {
            switch (cur) {
                case '\t':
                    builder.Append(@"\t");
                    break;
                case '\r':
                    builder.Append(@"\r");
                    break;
                case '\n':
                    builder.Append(@"\n");
                    break;
                case '"':
                    builder.Append(@"\""");
                    break;
                case '\'':
                    builder.Append(@"\'");
                    break;
                default:
                    builder.Append(cur);
                    break;
            }
        }
        return builder.ToString();
    }
}

public class RagdollCollisionElement
{
    public Rigidbody Rigidbody;
    public List<Collider> Colliders;
    public Joint Joint;
}

public static class Vector3Converter
{
    public static Vector3D ToVector3D(this Vector3 vector3) {
        return new Vector3D(vector3.x, vector3.y, vector3.z);
    }

    public static QuaternionD ToQuaternionD(this Quaternion q) {
        return new QuaternionD(q.w, q.x, q.y, q.z);
    }
}