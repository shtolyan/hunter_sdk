﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class TerrainImageExporter
{
    [MenuItem("Zen Modules/Data exporter/Export TopToDown picture of current scene")]
    public static void ExportTerrainPicture() {
        var terrain = Terrain.activeTerrain;
        var terrainMetadata = TerrainMetadataExporter.CreateMetadata(terrain);
        var newCameraGo = new GameObject();

        RenderSettings.fog = false;
        RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;

        var camera = newCameraGo.AddComponent<Camera>();
        camera.orthographic = true;
        camera.orthographicSize = (float)Math.Max(terrainMetadata.SizeMeters.X, terrainMetadata.SizeMeters.Y) / 2;

        var camPosition = -terrainMetadata.ZeroPointShiftMeters + terrainMetadata.SizeMeters/2;
        camera.transform.position = new Vector3((float)camPosition.X, 1000, (float)camPosition.Y);

        var camLookAtPosition = camera.transform.position;
        camLookAtPosition.y = 0;
        camera.transform.LookAt(camLookAtPosition);

        camera.nearClipPlane = 10;
        camera.farClipPlane = 2000;
        camera.targetTexture = new RenderTexture(6144, 6144, 24);        //баг https://issuetracker.unity3d.com/issues/ugui-in-texture2d-is-different-than-in-the-game-view-when-calling-totexture2d-method-on-a-rendertexture
        // camera.targetTexture = new RenderTexture(4096*2, 4096*2, 24);
        camera.forceIntoRenderTexture = true;

        camera.backgroundColor = new Color(0.235f, 0.235f, 0.235f);
        camera.clearFlags = CameraClearFlags.SolidColor;

        camera.cullingMask = LayerMask.GetMask("Default", "Water");

        camera.Render();
        
        try {
            SaveRTToFile(camera.targetTexture,terrainMetadata);
        }
        catch(Exception ex) {
            Debug.LogError(ex);
        }

        UnityEngine.Object.DestroyImmediate(newCameraGo);
    }

    private static void SaveRTToFile(RenderTexture rt, TerrainMetadata terrainMetadata) {
        RenderTexture.active = rt;

        var tex = CropEmptySpace(rt, terrainMetadata);

        RenderTexture.active = null;

        byte[] bytes;
        bytes = tex.EncodeToPNG();

        string path = string.Format("{0}/{1}.{2}.png", Constants.TerrainsExportPath, Constants.AssetBundleName, SceneManager.GetActiveScene().name.ToLowerInvariant());
        string fullPath = Path.GetFullPath(path);
        File.WriteAllBytes(fullPath, bytes);

        Debug.Log("Terrain picture was saved to\n" + fullPath);
    }    

    private static Texture2D CropEmptySpace(RenderTexture targetTexture, TerrainMetadata terrainMetadata) {
        var ratio = terrainMetadata.SizeMeters.X/terrainMetadata.SizeMeters.Y;

        var copyWidth  = ratio>=1 ? targetTexture.width : (int)(targetTexture.width*ratio);
        var copyHeight = ratio<=1 ? targetTexture.height : (int)(targetTexture.height/ratio);
        
        var offsetX = (targetTexture.width-copyWidth)/2;
        var offsetY= (targetTexture.height-copyHeight)/2;

        
        Texture2D croppedTexture = new Texture2D(copyWidth, copyHeight, TextureFormat.RGB24, false);
        croppedTexture.ReadPixels(new Rect(offsetX,offsetY,copyWidth,copyHeight), 0, 0);
        return croppedTexture;
    }
}