﻿using UnityEngine;

public class CollisionLayerInfo : MonoBehaviour{
    public enum RigidBodyGroup : short {
        AllFilter = -1,
        None = 0,
        Terrain = 1 << 0,
        Water = 1 << 1,
        Buildings = 1 << 2,
        Human = 1 << 3,
        Wheels = 1 << 4,
        Targets = 1 << 5,
        Vehicles = 1 << 6,
        BulletRaycsat = 1 << 7,
        PlacerMoverRaycast = 1 << 8,
        Ragdoll = 1 << 9,
        RagdollDead = 1 << 10,

    }
    public RigidBodyGroup CollisionLayer;    
}
