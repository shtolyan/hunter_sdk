﻿using System.IO;
using UnityEngine;

public class ScreenshotMaker : MonoBehaviour
{
    public int resWidth = 2048;
    public int resHeight = 2048;


    private bool takeHiResShot = false;

    public static string ScreenShotName(int width, int height, string sceneName) {
        return string.Format("{0}/../screenshots/screen_{1}x{2}_{3}.png",
            Application.dataPath,
            width, height, sceneName ?? System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

    public void TakeHiResShot() {
        takeHiResShot = true;
    }

    private void LateUpdate() {
        takeHiResShot |= Input.GetKeyDown(KeyCode.F6);
        if (takeHiResShot) {
            TakeScreenShot(null);
        }
    }

    public void TakeScreenShot(string sceneName) {
        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        var cam = GetComponent<Camera>();
        cam.targetTexture = rt;
        var mask = 1 << LayerMask.NameToLayer("Collision")
                   | 1 << LayerMask.NameToLayer("CollisionFlat")
                   | 1 << LayerMask.NameToLayer("CollisionFlatSand")
                   | 1 << LayerMask.NameToLayer("CubeFlatAspalt")
            ;
        cam.cullingMask = cam.cullingMask & ( ~mask );

        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        GetComponent<Camera>()
            .Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        GetComponent<Camera>()
            .targetTexture = null;
        RenderTexture.active = null;
        DestroyImmediate(rt);
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = ScreenShotName(resWidth, resHeight, sceneName);
        System.Diagnostics.Debug.Assert(filename != null, "filename != null");
        string path = Path.GetDirectoryName(filename);
        System.Diagnostics.Debug.Assert(path != null, "path != null");
        Directory.CreateDirectory(path);
        File.WriteAllBytes(filename, bytes);
        Debug.Log(string.Format("Took screenshot to: {0}", filename));
        takeHiResShot = false;
    }
}