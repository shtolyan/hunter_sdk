﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using UnityEditor;
using UnityEngine;

public class ScriptFinder : EditorWindow
{
    struct PrefabInfos
    {
        public string Name;
        public string Path;
    }

    static readonly List<PrefabInfos> PrefabNames = new List<PrefabInfos>();
    static readonly List<string> FilesWithScirpt = new List<string>();
    static int ProcessedFiles;

    [MenuItem("Zen Modules/Project/Find script in prefabs", true, 10)]
    static bool MenuItemCheck()
    {
        return Selection.activeObject is MonoScript;
    }

    static void SearchInFiles(string[] files, int start, int count, string guid)
    {
        byte[] buffer = new byte[5];
        string strToFind = "guid: " + guid;
        byte[] hexGuid = new byte[guid.Length / 2];

        for (int i = 0; i < guid.Length; i += 2)
        {
            hexGuid[i/2] = byte.Parse(new string(new [] { guid[i+1], guid[i] }), NumberStyles.HexNumber);
        }

        for (int i = start; i < start + count; i++)
        {
            using (var fileStream = new FileStream(files[i], FileMode.Open))
            {
                int readCount = fileStream.Read(buffer, 0, 5);
                if (readCount < 5)
                {
                    continue;
                }
                if (Encoding.ASCII.GetString(buffer) == "%YAML")
                {
                    using (var stream = new StreamReader(fileStream))
                    {
                        while (!stream.EndOfStream)
                        {
                            if (stream.ReadLine().Contains(strToFind))
                            {
                                lock (FilesWithScirpt)
                                {
                                    FilesWithScirpt.Add(files[i]);
                                }
                                break;
                            }
                        }
                    }
                }
                else
                {
                    byte[] binaryData = new byte[fileStream.Length];
                    fileStream.Seek(0, SeekOrigin.Begin);
                    fileStream.Read(binaryData, 0, binaryData.Length);
                    for (int j = 0; j < binaryData.Length - hexGuid.Length; j++)
                    {
                        int k;
                        for (k = 0; k < hexGuid.Length; k++)
                        {
                            if (binaryData[j + k] != hexGuid[k])
                                break;
                        }
                        if (k == hexGuid.Length)
                        {
                            lock (FilesWithScirpt)
                            {
                                FilesWithScirpt.Add(files[i]);
                            }
                            break;
                        }
                    }
                }
            }

            Interlocked.Increment(ref ProcessedFiles);
        }
    }

    [MenuItem("Zen Modules/Project/Find script in prefabs", false, 10)]
    static void MenuItem()
    {
        var script = (MonoScript)Selection.activeObject;
        var guidFind = Selection.assetGUIDs[0];
        var files = Directory.GetFiles("Assets", "*.prefab", SearchOption.AllDirectories);
        ProcessedFiles = 0;
        FilesWithScirpt.Clear();
        PrefabNames.Clear();

        Debug.Log("FindSciprt. GUID: " + guidFind);

        int processorCount = Environment.ProcessorCount;
        if (files.Length < processorCount)
        {
            SearchInFiles(files, 0, files.Length, guidFind);
        }
        else
        {
            int filesToCore = files.Length / processorCount;
            int remainFiles = files.Length % processorCount;

            Debug.LogFormat("FindSciprt. Cores: {0}, Files: {1}, FilesPerCore: {2}, Remaining: {3}", processorCount, files.Length, filesToCore, remainFiles);

            for (int i = 0; i < processorCount; i++)
            {
                int currentPosition = i * filesToCore;

                if (i == processorCount - 1)
                {
                    ThreadPool.QueueUserWorkItem(arg => SearchInFiles(files, currentPosition, filesToCore + remainFiles, guidFind));
                }
                else
                {
                    ThreadPool.QueueUserWorkItem(arg => SearchInFiles(files, currentPosition, filesToCore, guidFind));
                }
            }
        }

        while (ProcessedFiles < files.Length)
        {
            EditorUtility.DisplayProgressBar("Find usages", "File: " + script.name, (float)ProcessedFiles / files.Length);
            Thread.Sleep(1);
        }
        EditorUtility.ClearProgressBar();

        for (int i = 0; i < FilesWithScirpt.Count; i++)
        {
            var prefabPath = FilesWithScirpt[i];
            var strWihtouPath = prefabPath.Substring(prefabPath.LastIndexOf("\\", StringComparison.InvariantCulture) + 1);
            PrefabNames.Add(new PrefabInfos
            {
                Name = strWihtouPath.Substring(0, strWihtouPath.IndexOf(".", StringComparison.InvariantCulture)) + " ",
                Path = prefabPath
            });
        }

        if (PrefabNames.Count > 0)
        {
            ScriptFinder window = GetWindow<ScriptFinder>();
            window.Show();
        }
        else
        {
            EditorUtility.DisplayDialog("Find usages", "Nothing found", "OK");
        }
    }

    Vector2 scrollPosition;

    void OnGUI()
    {
        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
        for (int i = 0; i < PrefabNames.Count; i++)
        {
            if (GUILayout.Button(PrefabNames[i].Name, GUILayout.Height(20), GUILayout.ExpandWidth(true)))
            {
                Selection.activeGameObject = AssetDatabase.LoadAssetAtPath<GameObject>(PrefabNames[i].Path);
            }
        }
        EditorGUILayout.EndScrollView();
    }
}