using UnityEngine;

public static class Vector2Extension
{

    public static Vector2 Multiply(this Vector2 vector, float x, float y)
    {
        return new Vector2(vector.x * x, vector.y * y);
    }

    public static Vector2 Multiply(this Vector2 vector, float value)
    {
        return new Vector2(vector.x * value, vector.y * value);
    }

    public static Vector2 Add(this Vector2 vector, float x, float y)
    {
        return new Vector2(vector.x + x, vector.y + y);
    }

    public static Vector2 SetX(this Vector2 vector, float value)
    {
        vector.x = value;
        return vector;
    }

    public static Vector2 SetY(this Vector2 vector, float value)
    {
        vector.y = value;
        return vector;
    }

    public static Vector3 ToVector3(this Vector2 vector)
    {
        return new Vector3(vector.x, 0f, vector.y);
    }

    public static float Cross(this Vector2 vector, Vector2 other)
    {
        return vector.x * other.y - vector.y * other.x;
    }

    public static Vector2 Rotate(this Vector2 v, float angleDeg) {
        var sin = Mathf.Sin(angleDeg * Mathf.Deg2Rad);
        var cos = Mathf.Cos(angleDeg * Mathf.Deg2Rad);
         
        var tx = v.x;
        var ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

}