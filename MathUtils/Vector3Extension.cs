using UnityEngine;

public static class Vector3Extension
{

    public static Vector3 Multiply(this Vector3 vector, Vector3 another) {
        return new Vector3(vector.x * another.x, vector.y * another.y, vector.z * another.z);
    }

    public static Vector3 Multiply(this Vector3 vector, float x, float y, float z)
    {
        return new Vector3(vector.x * x, vector.y * y, vector.z * z);
    }

    public static Vector3 Multiply(this Vector3 vector, float value)
    {
        return new Vector3(vector.x * value, vector.y * value, vector.z * value);
    }

    public static Vector3 Add(this Vector3 vector, float x, float y, float z)
    {
        return new Vector3(vector.x + x, vector.y + y, vector.z + z);
    }

    public static Vector3 SetX(this Vector3 vector, float value)
    {
        vector.x = value;
        return vector;
    }

    public static Vector3 SetY(this Vector3 vector, float value)
    {
        vector.y = value;
        return vector;
    }

    public static Vector3 SetZ(this Vector3 vector, float value)
    {
        vector.z = value;
        return vector;
    }

    public static Vector3 AddX(this Vector3 vector, float value) {
        vector.x += value;
        return vector;
    }

    public static Vector3 AddY(this Vector3 vector, float value) {
        vector.y += value;
        return vector;
    }

    public static Vector3 AddZ(this Vector3 vector, float value) {
        vector.z += value;
        return vector;
    }

    public static Vector2 ToVector2(this Vector3 vector)
    {
        return new Vector2(vector.x, vector.z);
    }

}